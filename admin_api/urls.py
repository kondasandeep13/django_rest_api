from django.contrib import admin
from django.urls import path,include
from admin_api import views

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('admin_api',views.GroupView, basename = 'admin_api')

urlpatterns = [
    path('',views.index),
    
    path('api/groups',views.GroupInfoView.as_view()),
    #path('api/all',views.GroupView.as_view({'get': 'list'}))
    path('api/all/', include(router.urls))
]