from rest_framework import serializers
from admin_api.models import GroupInfo

class GroupInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupInfo
        fields = ["Name","Created"]