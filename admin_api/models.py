from django.db import models

# Create your models here.
class GroupInfo(models.Model):
    """Model definition for MODELNAME."""

    # TODO: Define fields here
    Name = models.CharField(max_length=256, primary_key = True)
    Created = models.DateTimeField(auto_now_add = True)
    Modified = models.DateTimeField(auto_now = True,null=True, blank=True,)


    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.Name
        
