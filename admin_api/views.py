from django.shortcuts import render
from django.http import HttpResponse
from admin_api.serializer import GroupInfoSerializer
from rest_framework import viewsets
from admin_api.models import GroupInfo

from rest_framework.views import APIView
from rest_framework.response import Response

from rest_framework import status

# Create your views here.

def index(request):
    return HttpResponse("Hello World!")

class GroupView(viewsets.ModelViewSet):
    queryset = GroupInfo.objects.all()
    serializer_class = GroupInfoSerializer


class GroupInfoView(APIView):

    def get(self, request):
        groups = GroupInfo.objects.all()
        serializer = GroupInfoSerializer(groups, many=True)
        return Response(serializer.data)

    def post(self,request):
        groups = request.data
        print(groups)
        serializer = GroupInfoSerializer(data=groups)
        if serializer.is_valid(raise_exception=True):
            groupNameSaved = serializer.save()
        return Response({"message":"Sucesfully created"})


    # def put(self,request):
    #     groups = request.data['Name']
    #     print(groups)
    #     serializer = GroupInfoSerializer(data=groups)
    #     if serializer.is_valid(raise_exception=True):
    #         groupNameSaved = serializer.save()
    #     return Response({"message":"Sucesfully created"})

    def delete(self,request):
        name = request.data.get('Name')
        print("1",name)
        #g_name = GroupInfo.objects.get(Name=name)
        #print("2",g_name)
        try:
            if GroupInfo.objects.get(Name=name):
                GroupInfo.objects.get(Name=name).delete()
                return Response({"Message":"Data is deleted"},status=status.HTTP_404_NOT_FOUND)
        except:
            return Response({"message": "Object is not present in Database"},status=status.HTTP_404_NOT_FOUND)
            